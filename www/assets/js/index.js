          // PAGE DOWNLOADING 

var myVar;

function loadFunction() {

	$( function() {
      var progressbar = $( "#progressbar" ),
        progressLabel = $( ".progress-label" );
   
      progressbar.progressbar({
        value: false,
        change: function() {
          progressLabel.text( progressbar.progressbar( "value" ) + "%" );
        },
        complete: function() {
          progressLabel.text( "Complete!" );
        }
      });
   
      function progress() {
        var val = progressbar.progressbar( "value" ) || 0;
   
        progressbar.progressbar( "value", val + 8 );
   
        if ( val < 99 ) {
          setTimeout( progress, 80 );
        }
      }
   
      setTimeout( progress, 600 );
  } );


    myVar = setTimeout(showPage, 2500);
}

function showPage() {	
	// document.getElementById("loader").style.display = "none";	
	document.getElementById("loading-page").style.display = "none";
  document.getElementById("myModal").style.display = "none";
	document.getElementById("header").style.display = "block";
	document.getElementById("main").style.display = "block";
	document.getElementById("footer").style.display = "block";
}


          // THIS IS SCROLL FIXED

$(window).scroll(function(){
	if ($(this).scrollTop() > 62) {
          $('#second').addClass('head-second-row');
   

  		} else {
	        $('#second').removeClass('head-second-row');
	  
          
 		}
});


// THIS IS SHAKING EFFECT

function periodical() {
  $('#shaker').effect('shake', { times:3 }, 200);
}
$(document).ready(function() {
  $('#shaker').hide().css('display','').fadeIn(600);
  var shake = setInterval(periodical, 7500);
  
  /* updated: click clear */
  $('#shaker').click(function() {
    clearInterval(shake);
  })  
});



// THIS IS FOR DROPDOWN EFFECT


var dropdownSelectors = $('.dropdown, .dropup');

// Custom function to read dropdown data
// =========================
function dropdownEffectData(target) {
  // @todo - page level global?
  var effectInDefault = null,
      effectOutDefault = null;
  var dropdown = $(target),
      dropdownMenu = $('.dropdown-menu', target);
  var parentUl = dropdown.parents('div.col-md-3'); 

  // If parent is ul.nav allow global effect settings
  if (parentUl.size() > 0) {
    effectInDefault = parentUl.data('dropdown-in') || null;
    effectOutDefault = parentUl.data('dropdown-out') || null;
  }
  
  return {
    target:       target,
    dropdown:     dropdown,
    dropdownMenu: dropdownMenu,
    effectIn:     dropdownMenu.data('dropdown-in') || effectInDefault,
    effectOut:    dropdownMenu.data('dropdown-out') || effectOutDefault,  
  };
}

// Custom function to start effect (in or out)
// =========================
function dropdownEffectStart(data, effectToStart) {
  if (effectToStart) {
    data.dropdown.addClass('dropdown-animating');
    data.dropdownMenu.addClass('animated');
    data.dropdownMenu.addClass(effectToStart);    
  }
}

// Custom function to read when animation is over
// =========================
function dropdownEffectEnd(data, callbackFunc) {
  var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
  data.dropdown.one(animationEnd, function() {
    data.dropdown.removeClass('dropdown-animating');
    data.dropdownMenu.removeClass('animated');
    data.dropdownMenu.removeClass(data.effectIn);
    data.dropdownMenu.removeClass(data.effectOut);
    
    // Custom callback option, used to remove open class in out effect
    if(typeof callbackFunc == 'function'){
      callbackFunc();
    }
  });
}

// Bootstrap API hooks
// =========================
dropdownSelectors.on({
  "show.bs.dropdown": function () {
    // On show, start in effect
    var dropdown = dropdownEffectData(this);
    dropdownEffectStart(dropdown, dropdown.effectIn);
  },
  "shown.bs.dropdown": function () {
    // On shown, remove in effect once complete
    var dropdown = dropdownEffectData(this);
    if (dropdown.effectIn && dropdown.effectOut) {
      dropdownEffectEnd(dropdown, function() {}); 
    }
  },  
  "hide.bs.dropdown":  function(e) {
    // On hide, start out effect
    var dropdown = dropdownEffectData(this);
    if (dropdown.effectOut) {
      e.preventDefault();   
      dropdownEffectStart(dropdown, dropdown.effectOut);   
      dropdownEffectEnd(dropdown, function() {
        dropdown.dropdown.removeClass('open');
      }); 
    }    
  }, 
});
