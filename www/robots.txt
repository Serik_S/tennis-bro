User-agent: Yandex
Disallow: /admin
Disallow: /img
Host: tennis-bro.kz

User-agent: Googlebot
Disallow: /admin
Disallow: /img

User-agent: *
Disallow: /admin
Disallow: /img 

Sitemap: http://tennis-bro.kz/sitemap.xml