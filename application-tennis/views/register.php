<html>
	<head>
	    <link rel="stylesheet" href=" <?php echo base_url("assets/bootstrap/dist/css/bootstrap.css"); ?>">    
		<link rel="stylesheet" href=" <?php echo base_url("assets/css/registration.css"); ?> ">
		<title> <?php echo $title; ?> </title>
	</head>
	<body>
		<div class="register-container container">
            <div class="row">
            	<div class="col-md-6">
	            	<div class="register span6">
	                    
	                    <?php $attributes = array("name" => "signupform");
					      echo form_open("signup/index", $attributes);?>
					      <br>
					      <h2><span class="red"><strong>Присоединяйтесь :-) </strong></span></h2>           
					      
					      <div class="form-group">
					        <label for="name"> Имя </label>
					        <input class="form-control" name="fname" placeholder="Как вас зовут?" type="text" value="<?php echo set_value('fname'); ?>" />
					        <span class="text-danger"><?php echo form_error('fname'); ?></span>
					      </div>      
					    
					      <div class="form-group">
					        <label for="name">Фамилия </label>
					        <input class="form-control" name="lname" placeholder="Ваша фамилия?" type="text" value="<?php echo set_value('lname'); ?>" />
					        <span class="text-danger"><?php echo form_error('lname'); ?></span>
					      </div>
					    
					      <div class="form-group">
					        <label for="email">Эл. Почта</label>
					        <input class="form-control" name="email" placeholder="Введите электронную почту..." type="text" value="<?php echo set_value('email'); ?>" />
					        <span class="text-danger"><?php echo form_error('email'); ?></span>
					      </div>

					      <div class="form-group">
					        <label for="subject">Пароль</label>
					        <input class="form-control" name="password" placeholder="Введите пароль..." type="password" />
					        <span class="text-danger"><?php echo form_error('password'); ?></span>
					      </div>

					      <div class="form-group">
					        <label for="subject">Подтверждение</label>
					        <input class="form-control" name="cpassword" placeholder="Подтвердите ваш пароль..." type="password" />
					        <span class="text-danger"><?php echo form_error('cpassword'); ?></span>
					      </div>

					      <div class="form-group">
					        <button name="submit" type="submit" class="btn btn-info">Зарегистрироваться</button>
					        
					      </div>
					      <?php echo form_close(); ?>
					      <?php echo $this->session->flashdata('msg'); ?>
	                </div>	
            	</div>                
                <div class="col-md-6">	
                		<img class="image-registration" src=" <?php echo base_url("assets/images/keep-calm.png"); ?> " alt="Не найдено :-(">
                </div>
            </div>
        </div>
	</body>
</html>
