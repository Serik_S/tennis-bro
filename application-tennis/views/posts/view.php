<html>
	<head>
		<link rel="stylesheet" href=" <?php echo base_url("assets/css/view.css"); ?>">
	</head>
	<body>
		<div class="container marged">
			<div class="row">
				<div class="col-md-8">
					<h2 class="news-title"> <?php echo $posts_item['Post-title']; ?> </h2>

					<p class="news-text">
						<?php echo $posts_item['Post-text']; ?>
					</p>
				</div>
				<div class="col-md-4">
						<?php 
							$name_of_image = strval($posts_item['image']);
							$url = "assets/images/";
							$uri = $url . '' . $name_of_image;
						?>	
					<?php if(strlen($name_of_image) > 1) : ?>
						<img vspace="5" hspace="5" class="img-responsive news-image" src=" <?php echo base_url("assets/images/$name_of_image"); ?>" alt="image not found">
					<?php endif; ?>
				</div>
			</div>
		</div>
	</body>
</html>