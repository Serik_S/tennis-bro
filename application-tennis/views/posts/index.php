<html>
	<head>

		<!-- Mail RU WEBMASTER -->
		<meta name='wmail-verification' content='cc409fa30e688e18298055060922ead3' />

		<!-- Yandex WEBMASTER -->
		<meta name="yandex-verification" content="d06de9b2b46b331d" />
		
		<!-- Google WEBMASTER -->
		<meta name="google-site-verification" content="8VsG5abMbIoeFiYq2UlvMFHcTQs6BAV_o1Q9N3N6vOs" />

		<!-- Bing Webmaster -->
		<meta name="msvalidate.01" content="094CF5BE192CB685A23F839D05163E67" />

		<!-- MyWot -->
		<meta name="wot-verification" content="6adeaa8fb227ccba41a4"/>




		<link rel="shortcut icon" href=" <?php echo base_url("assets/images/icon.png"); ?> ">
    	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    
	    <meta property="og:title" content="Tennis Bro" />
		<meta property="og:type" content="Теннисный блог" />
		<meta property="og:url" content="http://tennis-bro.kz" />
		<meta property="og:image" content="http://tennis-bro.kz/assets/images/background.jpg" />

	    <meta name="description" content="Теннисный блог посвященный любителям и профессиональным тенниситам. Присоединяйтесь к нам, приглашайте друзей и получайте море полезной информации о теннисе. Предлагайте свои идеи по улучшению нашего блога, почаще пишите комментарии и ставьте лайки!">
	    <meta name="keywords" content="Cвежие новости, рейтинги, и небольшое описание о сайте">
		
	    <meta name="author" content="Serik-S">

        <title> <?php echo $title; ?> </title>

	    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" href=" <?php echo base_url("assets/font-awesome/css/font-awesome.css"); ?>">
	    <link rel="stylesheet" href=" <?php echo base_url("assets/bootstrap/dist/css/bootstrap.min.css"); ?>">    							<link rel="stylesheet" href=" <?php echo base_url("assets/css/animate.css"); ?>">
		<link rel="stylesheet" href=" <?php echo base_url("assets/css/style.css"); ?> ">

		<!-- Yandex.Metrika counter -->
		<script type="text/javascript">
		    (function (d, w, c) {
		        (w[c] = w[c] || []).push(function() {
		            try {
		                w.yaCounter44211814 = new Ya.Metrika({
		                    id:44211814,
		                    clickmap:true,
		                    trackLinks:true,
		                    accurateTrackBounce:true
		                });
		            } catch(e) { }
		        });

		        var n = d.getElementsByTagName("script")[0],
		            s = d.createElement("script"),
		            f = function () { n.parentNode.insertBefore(s, n); };
		        s.type = "text/javascript";
		        s.async = true;
		        s.src = "https://mc.yandex.ru/metrika/watch.js";

		        if (w.opera == "[object Opera]") {
		            d.addEventListener("DOMContentLoaded", f, false);
		        } else { f(); }
		    })(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript><div><img src="https://mc.yandex.ru/watch/44211814" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		<!-- /Yandex.Metrika counter -->
		
		<!-- Google Analytics -->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-86672749-2', 'auto');
		  ga('send', 'pageview');
		</script>
		<!-- /Google Analytics -->

		<!-- Rating@Mail.ru counter -->
		<script type="text/javascript">
		var _tmr = window._tmr || (window._tmr = []);
		_tmr.push({id: "2895840", type: "pageView", start: (new Date()).getTime()});
		(function (d, w, id) {
		  if (d.getElementById(id)) return;
		  var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
		  ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
		  var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
		  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
		})(document, window, "topmailru-code");
		</script><noscript><div>
		<img src="//top-fwz1.mail.ru/counter?id=2895840;js=na" style="border:0;position:absolute;left:-9999px;" alt="" />
		</div></noscript>
		<!-- //Rating@Mail.ru counter -->
	</head>
	
	<body>	


		<header>
		<div class="header">
			<div class="container-fluid first-menu-padd">
				<div class="row">
					<div class="col-md-8"></div>
					<div class="col-md-4">
						<menu class="first-menu">
							<ul>
								<li>
									 <a href=""> <span class="glyphicon glyphicon-th-list"></span> МЕНЮ</a>
								</li>
								<li>
									<a href=""> <span class="glyphicon glyphicon-search"></span> ПОИСК</a>
								</li>
								<li>
									<a href=" <?php echo site_url('login'); ?> "><span class="glyphicon glyphicon-user"></span> ВОЙТИ</a>
								</li>
							</ul>
						</menu>
					</div>
				</div>
			</div>
				<div id="second" class="container-fluid">
					<div class="row">
						<div class="col-md-4">
							<a class="marged-heading" href="<?php echo site_url('') ?>">
								<img class="icon" src=" <?php echo base_url("assets/images/ball-icon.png"); ?>" alt="">
								<h1 class="main-heading">Tennis Bro</h1>
							</a>
						</div>
						<div class="col-md-3"></div>
						<div class="col-md-5">
							<menu class="second-menu">
								<ul>
										<li>
											<a href="<?php echo site_url('') ?>"  <?php if($this->uri->segment(1)==""){echo 'class="active"';} ?> >    	Главная
											</a>
										</li>
										<li>
											<a href="<?php echo site_url('articles') ?>" <?php if($this->uri->segment(1)=="articles"){echo 'class="active"';} ?> >
												Интересные Статьи
											</a>
										</li>
										<li>
											<a href="<?php echo site_url('about') ?>" <?php if($this->uri->segment(1)=="about"){echo 'class="active"';} ?> >
												О Нас
											</a>
										</li>
										<li>
											<a href="<?php echo site_url('videos') ?>" <?php if($this->uri->segment(1)=="videos"){echo 'class="active"';} ?> >
												Видео
											</a>
										</li>	
								</ul>
							</menu>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row second-menu-padd">
						<div class="col-md-2"></div>
						<div class="col-md-8">
							<h2 class="head-text">Любите Теннис, где бы вы ни находились.</h2>
							<p class="head-subtext">Теннисный блог посвященный любителям и профессиональным тенниситам. Присоединяйтесь к нам, приглашайте друзей и получайте море полезной информации о теннисе. Предлагайте свои идеи по улучшению нашего блога, почаще пишите комментарии и ставьте лайки!
							</p>
						</div>
						<div class="col-md-2"></div>
					</div>
					<!-- <div class="row">
						<div class="col-md-5"></div>
						<div class="col-md-1">
							<a href="" class="btn btn-info btn-lg tennis-info">
							Начать
							</a>	
						</div>
						<div class="col-md-6"></div>
					</div> -->
			</div>
		</div>
	</header>

		<!-- THIS IS MAIN PART -->
		<div id="main" class="container animate-bottom" >
			<div id="nopadd" class="row nopadd">
				<div class="col-md-12 nopadd">
					

					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					  <!-- Indicators -->
					  <ol class="carousel-indicators">
					    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
					    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
					  </ol>

					  <!-- Wrapper for slides -->
					  <div class="carousel-inner" role="listbox">
					    <div class="item active">
					      <img src="<?php echo base_url("assets/images/carousel1.jpg"); ?>" class="image-responsive image-carousel" alt="...">
					      <div class="carousel-caption label-caption">
					        Участвуйте в турнирах
					      </div>
					    </div>
					    <div class="item">
					      <img src="<?php echo base_url("assets/images/carousel2.jpg"); ?>" class="image-responsive image-carousel" alt="...">
					      <div class="carousel-caption label-caption">
					        Покупайте лучшую экипировку
					      </div>
					    </div>
					    <div class="item">
					      <img src="<?php echo base_url("assets/images/carousel3.jpg"); ?>" class="image-responsive image-carousel" alt="...">
					      <div class="carousel-caption label-caption">
					        Становитесь Чемпионами
					      </div>
					    </div>
					  </div>

					  <!-- Controls -->
					  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
					    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					    <span class="sr-only">Previous</span>
					  </a>
					  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
					    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					    <span class="sr-only">Next</span>
					  </a>
					</div>






				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<div class="row">
						<h3 class="main-heading">
							Последние Новости
						</h3>	
					</div>
					<div class="row">
						

						<?php foreach ($posts as $post_item): ?>
							<div class="news">
								<?php 
									$name_of_image = strval($post_item['image']);
									$url = "assets/images/";
									$uri = $url . '' . $name_of_image;

								?>
								<?php if(strlen($name_of_image) > 1) : ?>
									<img class="img-responsive news-image" src=" <?php echo base_url("assets/images/$name_of_image"); ?>" alt="image not found">
								<?php endif; ?>
								
								<a href="" class="news-label"> <?php echo $post_item['Post-title']; ?> </a>
						        <div class="">
						        		<?php $show = substr($post_item['Post-text'], 0, 100) . "..."; ?>
						                <?php echo $show   ?>
						        </div>
						        <p>
						        <a href="<?php echo site_url('posts/'.$post_item['Slug']); ?>">Прочитать</a></p>	
						        <p class="publisher"> <?php echo $post_item['Post-Author']; ?> </p>
						        <hr>
							</div>        
						<?php endforeach; ?>

					
					</div>
				</div>
				<div class="col-md-4">
					<h4 align="center">Рейтинг ATP</h4>
					<table class="table">
						<tr>
							<th>#</th>
							<th>Игроки</th>
							<th>Очки</th>	
						</tr>
						<tr>
							<td>1</td>
							<td>Энди Маррей</td>
							<td>11540</td>
						</tr>
						<tr>
							<td>2</td>
							<td>Новак Джокович</td>
							<td>9825</td>
						</tr>
						<tr>
							<td>3</td>
							<td>Стэн Вавринка</td>
							<td>5695</td>
						</tr>

					</table>
					<h4 align="center">Рейтинг WTA</h4>

					<table class="table">
						<tr>
							<th>#</th>
							<th>Игроки</th>
							<th>Очки</th>	
						</tr>
						<tr>
							<td>1</td>
							<td>Серена Уильямс</td>
							<td>7780</td>
						</tr>
						<tr>
							<td>2</td>
							<td>Анжелик Кербер</td>
							<td>7115</td>
						</tr>
						<tr>
							<td>3</td>
							<td>Каролина Плишкова</td>
							<td>5270</td>
						</tr>

					</table>

					<h3>Результаты</h3>

					<h4 align="center" class="h-text">КУБОК ДЭВИСА ОДИНОЧКИ</h4>
					<h5 align="center" class="under-h-text">Мировая Лига</h5>
					<table class="table">
						<tr>
							<td>
								<img class="country-image" src="<?php echo base_url("assets/images/aus.gif"); ?>" alt="sorry... no image">
								Джордан Томпсон
								<span class="glyphicon glyphicon-ok"></span>
							</td>
							<td>6</td>
							<td>6</td>
							<td>6</td>
						</tr>
						<tr>
							<td>
								<img class="country-image" src="<?php echo base_url("assets/images/cz.png"); ?>" alt="sorry... no image">
								Иржи Веселы								
							</td>
							<td>3</td>
							<td>3</td>
							<td>4</td>
						</tr>
					</table>
					
					<h4 align="center"> WTA ТАЙБЭЙ </h4>

					<h5 align="center" class="under-h-text">1/4 Финала</h5>
					<table class="table">
						<tr>
							<td>
								<img src="<?php echo base_url("assets/images/jap.png"); ?>" class="country-image" alt="sorry... no image">
								Мисаки Дои							
							</td>
							<td>4</td>
							<td>3</td>
						</tr>
						<tr>
							<td>
								<img src="<?php echo base_url("assets/images/cz.png"); ?>" class="country-image" alt="sorry... no image">
								Люси Шафаржова
								<span class="glyphicon glyphicon-ok"></span>
							</td>
							<td>6</td>
							<td>6</td>
						</tr>
					</table> 

					<div id="shaker" class="advert">						
							<img class="image-responsive image-advert" src="<?php echo base_url("assets/images/advert.png"); ?>" alt="изображение не найдено :( ">
						 <a href="http://eatithealthy.kz/"> <p>Не забывайте вкусно и правильно питаться вместе с Гохой! </p></a>
					</div>


				</div>
			</div>
		</div>
		<!-- END OF MAIN PART -->


		<script></script>

	</body>
</html>