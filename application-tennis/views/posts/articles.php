<html>
	<head>
		<link rel="stylesheet" href=" <?php echo base_url("assets/css/articles.css"); ?> ">
	</head>
	<body>
		<div class="container main">
				<div class="row">
		            <div class="col-md-12">
		                <h1 class="page-header">Мои Статьи
		                    <small>Здесь все самое интересное :-)</small>
		                </h1>
		            </div>
	        	</div>
				<div class="row padding">
					<div class="col-md-6 portfolio-item">

						<a href="#">
		                    <img class="img-responsive" src="<?php echo base_url("assets/images/winner.jpg"); ?>" alt="">
		                </a>
		                <h3>
		                    <a href="#">Теннис - лучшая игра в мире</a>
		                </h3>
		                <p>Статья о том почему теннис является одним из самых зрелищных видов спорта</p>
		                <a href=" <?php echo site_url(""); ?> " class="btn btn-default">Читать</a>



		                
		            </div>
		            <div class="col-md-6 portfolio-item">
		                <a href="#">
		                    <img class="img-responsive" src="<?php echo base_url("assets/images/novak.jpg"); ?>" alt="">
		                </a>
		                <h3>
		                    <a href="article_interesting.html">Мой любимый теннисист</a>
		                </h3>
		                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae...</p>
		                <a class="btn btn-default">Читать</a>
		            </div>
				</div>

				<div class="row padding">
					<div class="col-md-6 portfolio-item">
		                <a href="#">
		                    <img class="img-responsive" src="<?php echo base_url("assets/images/melbourne.jpg"); ?>" alt="">
		                </a>
		                <h3>
		                    <a href="#">Australian Open</a>
		                </h3>
		                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae...</p>
		                <a class="btn btn-default">Читать</a>
		            </div>
		            <div class="col-md-6 portfolio-item">
		                <a href="#">
		                    <img class="img-responsive" src="<?php echo base_url("assets/images/roland.jpg"); ?>" alt="">
		                </a>
		                <h3>
		                    <a href="#">French Open - Roland Garros</a>
		                </h3>
		                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae...</p>
		                <a class="btn btn-default">Читать</a>
		            </div>

				</div>

				<div class="row padding">
					<div class="col-md-6 portfolio-item">
		                <a href="#">
		                    <img class="img-responsive" src="<?php echo base_url("assets/images/first.jpg"); ?>" alt="">
		                </a>
		                <h3>
		                    <a href="#">Первые среди первых!</a>
		                </h3>
		                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae...</p>
		                <a class="btn btn-default">Читать</a>
		            </div>
		            <div class="col-md-6 portfolio-item">
		                <a href="#">
		                    <img class="img-responsive" src="<?php echo base_url("assets/images/federer.jpg"); ?> " alt="">
		                </a>
		                <h3>
		                    <a href="#">Достижения Легендарного Роджера Федерера</a>
		                </h3>
		                <p>Этот человек действительно легендарный поскольку...</p>
		                <a class="btn btn-default">Читать</a>
		            </div>
				</div>
			</div>
	</body>
</html>