<html>
	<head>
	    <link rel="stylesheet" href=" <?php echo base_url("assets/bootstrap/dist/css/bootstrap.css"); ?>">    
	    <link rel="stylesheet" href=" <?php echo base_url("assets/css/post-create.css"); ?>">
	</head>
	<body>
		<div class="container">
			<div class="row">

				<div class="col-md-5 post">

				<h2><?php echo $title; ?></h2>

					<?php echo validation_errors(); ?>

					<?php echo form_open('posts/create'); ?>
					<form class="" action="" method="post">
			
						<div class="form-group">
							<label for="Post-title">Название Новости</label>
					   		<input class="form-control" type="input" name="Post-title" /><br />
						</div>
					    
						<div class="form-group">
							<label for="Post-text">Текст</label>
					    	<textarea class="form-control" name="Post-text"></textarea><br />
						</div>
					    
						<div class="form-group">
							<label for="Post-Author">Автор</label>
					    	<input class="form-control" type="input" name="Post-Author">
						</div>
					    
						<div class="form-group">
							<label for="Slug">Английское название для ссылки</label>
					    	<input type="input" class="form-control" name="Slug">
						</div>

						<div class="form-group">
							<label for="File">Добавьте изображение</label>
							<input type="file" name="fileToUpload" id="fileToUpload" data-filename-placement="inside">
						</div>
					    

					    <input class="btn btn-success" type="submit" name="submit" value="Опубликовать!" />

					</form>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
	</body>
</html>