<html>
	<head>
		<link rel="stylesheet" href=" <?php echo base_url("assets/css/videos.css"); ?> ">

		<meta name="keywords" content="Видео про теннис, эпичные моменты">
		<meta name="description" content="Данный раздел поможет вам расслабиться благодаря нашим видео роликам, так что просто смотрите и наслаждайтесь">
		<meta property="og:title" content="Видео про теннис" />

		<meta property="og:title" content="Tennis Bro" />
		<meta property="og:type" content="Теннисный блог" />
		<meta property="og:url" content="http://tennis-bro.kz" />
		<meta property="og:image" content="http://tennis-bro.kz/assets/images/federer.jpg" />

	</head>
	<body>

		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="page-header">Просто Смотрите
                    	<small>и наслаждайтесь :-)</small>
                	</h1>
				</div>
			</div>

			<div class="row padding">
				<div class="col-md-6 portfolio-item">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/c6TqM1Qu4c0" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="col-md-6 portfolio-item"> 
					<div class="video-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/A7Mf3huji4k" frameborder="0" allowfullscreen></iframe></div>
				</div>				
			</div>

			<div class="row padding"> 
				<div class="col-md-6 portfolio-item">
					<div class="video-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/_mdgTR5ZyDs" frameborder="0" allowfullscreen></iframe></div>
				</div>
				<div class="col-md-6 portfolio-item"> 
					<div class="video-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/krp7J6e0egQ" frameborder="0" allowfullscreen></iframe></div>
				</div>
			</div>

			<div class="row padding portfolio-item">
				<div class="col-md-6 portfolio-item">
					<div class="video-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/WyxGyePQDQc" frameborder="0" allowfullscreen></iframe></div>
				</div>
				<div class="col-md-6 portfolio-item"> 
					<div class="video-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/lSfrt2q4m6Q" frameborder="0" allowfullscreen></iframe></div>
				</div>	
			</div>
		</div>
	</body>
</html>