<html>
    <head>
		
    	<link rel="shortcut icon" href=" <?php echo base_url("assets/images/icon.png"); ?> ">
    	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="author" content="">
	    <meta name="keywords" content="Блог, Статьи, Видео, Информация о Нас">

	    
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href=" <?php echo base_url("assets/font-awesome/css/font-awesome.css"); ?>">
    <link rel="stylesheet" href=" <?php echo base_url("assets/bootstrap/dist/css/bootstrap.css"); ?>">    
    <link rel="stylesheet" href=" <?php echo base_url("assets/css/animate.css"); ?>">

    	<link rel="stylesheet" href=" <?php echo base_url("assets/css/header.css"); ?> ">

    	

        <title> <?php echo $title; ?> </title>

		<!-- Yandex.Metrika counter -->
		<script type="text/javascript">
		    (function (d, w, c) {
		        (w[c] = w[c] || []).push(function() {
		            try {
		                w.yaCounter44211814 = new Ya.Metrika({
		                    id:44211814,
		                    clickmap:true,
		                    trackLinks:true,
		                    accurateTrackBounce:true
		                });
		            } catch(e) { }
		        });

		        var n = d.getElementsByTagName("script")[0],
		            s = d.createElement("script"),
		            f = function () { n.parentNode.insertBefore(s, n); };
		        s.type = "text/javascript";
		        s.async = true;
		        s.src = "https://mc.yandex.ru/metrika/watch.js";

		        if (w.opera == "[object Opera]") {
		            d.addEventListener("DOMContentLoaded", f, false);
		        } else { f(); }
		    })(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript><div><img src="https://mc.yandex.ru/watch/44211814" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		<!-- /Yandex.Metrika counter -->

		<!-- Google Analytics -->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-86672749-2', 'auto');
		  ga('send', 'pageview');
		</script>
		<!-- /Google Analytics -->


		<!-- Rating@Mail.ru counter -->
		<script type="text/javascript">
		var _tmr = window._tmr || (window._tmr = []);
		_tmr.push({id: "2895840", type: "pageView", start: (new Date()).getTime()});
		(function (d, w, id) {
		  if (d.getElementById(id)) return;
		  var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
		  ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
		  var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
		  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
		})(document, window, "topmailru-code");
		</script><noscript><div>
		<img src="//top-fwz1.mail.ru/counter?id=2895840;js=na" style="border:0;position:absolute;left:-9999px;" alt="" />
		</div></noscript>
		<!-- //Rating@Mail.ru counter -->




    </head>
    <body>

    	<!-- This is Header PART -->
		<header>
			<div id="header" class="header-part animate-bottom" >
			<div class="container-fluid first-menu-padd">
				<div class="row">
					<div class="col-md-8"></div>
					<div class="col-md-4">
						<menu class="first-menu">
							<ul>
								<li>
									 <a href=""> <span class="glyphicon glyphicon-th-list"></span> МЕНЮ</a>
								</li>
								<li>
									<a href=""> <span class="glyphicon glyphicon-search"></span> ПОИСК</a>
								</li>
								<li>
									<a href="<?php echo site_url("login"); ?>"><span class="glyphicon glyphicon-user"></span> ВОЙТИ</a>
								</li>
							</ul>
						</menu>
					</div>
				</div>
			</div>
				<div id="second" class="container-fluid">	
					<div class="row">
						<div class="col-md-4">
							<a class="marged-heading" href="<?php echo site_url('') ?>">
								<img class="icon" src=" <?php echo base_url("assets/images/ball-icon.png"); ?>" alt="">
								<h1 class="main-heading">Tennis Bro</h1>
							</a>
						</div>
						<div class="col-md-3"></div>
						<div class="col-md-5">
							<menu class="second-menu" id="ddmenu">
								<ul>
										<li>
											<a href="<?php echo site_url('') ?>"  <?php if($this->uri->segment(1)==""){echo 'class="active"';} ?> >    	Главная
											</a>
										</li>
										<li>
											<a href="<?php echo site_url('articles') ?>" <?php if($this->uri->segment(1)=="articles"){echo 'class="active"';} ?> >
												Интересные Статьи
											</a>
										</li>
										<li>
											<a href="<?php echo site_url('about') ?>" <?php if($this->uri->segment(1)=="about"){echo 'class="active"';} ?> >
												О Нас
											</a>
										</li>
										<li>
											<a href="<?php echo site_url('videos') ?>" <?php if($this->uri->segment(1)=="videos"){echo 'class="active"';} ?> >
												Видео
											</a>
										</li>	
								</ul>
							</menu>


						</div>
					</div>
				</div>
			</div>
		</header>

		<!-- END OF HEADER -->

    

    </body>
</html>