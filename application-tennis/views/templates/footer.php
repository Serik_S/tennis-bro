<html>
	<head>
		<link rel="stylesheet" href=" <?php echo base_url("/assets/css/footer.css"); ?> ">
	</head>
	<body>
		<!-- THIS IS FOOTER PART -->
		<footer>
			<div id="footer" class="footer animate-bottom">
				<div class="container">
					<div class="row">
						<div class="col-md-4 special">
								<h3 class="social-h">Контакты</h3>
								<div class="row">
									<div class="col-md-12">
										<p class="footText">
											<span class="glyphicon glyphicon-home"></span>
												пр-т Абая 95/3, 050000, Алматы, Республика Казахстан
										</p>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">								
										<p class="footText">
											<span class="glyphicon glyphicon-envelope"></span>                  					
											 <a href="mailto:2909Serik@gmail.com?subject=Вопрос по сайту Tennis Bro">2909Serik@gmail.com</a>
										</p>
									</div>
								</div>
								<div class="row">	
										<div class="col-md-12">	
												<p class="footText">	
													<span class="glyphicon glyphicon-phone"></span>  
													<a href="skype:+77017363555"> +7 (701) 7363-555 </a> 
												</p>
										</div>
								</div>
								<div class="row">	
										<div class="col-md-12">	
												<p class="footText">	
													<span class="glyphicon glyphicon-phone"></span> 
													 <a href="skype:+77017868555"> +7 (701) 7868-555 </a> 
												</p>
										</div>
								</div>
								<div class="row">	
										<div class="col-md-12">	
												<p class="footText">	
													<span class="glyphicon glyphicon-earphone"></span>
													<a href="skype:+77273944381"> +7  (727) 394-43-81 </a> 
												</p>
										</div>
								</div>
							</div>
						<div class="col-md-1"></div>
						<div class="col-md-3">
							<h3 class="social-h">Помощь</h3>
							<ul class="myList">
								<li> 
									<a> <span class="glyphicon glyphicon-info-sign"></span>Информация</a>
								</li>
								<li>  
									<a><span class="glyphicon glyphicon-globe"></span>Служба поддержки</a> 
								</li>
								<li> 
									<a><span class="glyphicon glyphicon-question-sign"></span>Вопросы и ответы</a>       
								</li>
								<li>
									<a><span class="glyphicon glyphicon-pencil"></span>Блог</a>
								</li>
								<li>
									<a><span class="glyphicon glyphicon-comment"></span>Форум</a>
								</li>
							</ul>
						</div>
						<div class="col-md-4">	
							<div class="row">
								<div class="col-md-12">
									<h3 class="social-h" align="center">Расскажите о нас в Социальных сетях</h3>
									<menu>
										<ul class="foot-ul">
											<li class="changedPadd">
												<a href=" <?php echo $urlvk; ?> ">
													<i class="fa fa-vk iconPosVk"></i>				
												</a>
											</li>
											<li class="changedPadd">
												<a class="changedPadd" href=" <?php echo $urlfa; ?> ">
													<i class="fa fa-facebook iconPosFa"></i>
												</a>
											</li>
											<li class="changedPadd">
												<a class="changedPadd" href=" <?php echo $urlli; ?> ">
													<i class="fa fa-linkedin iconPosIn"></i>	
												</a>
											</li>
											<li class="changedPadd">
												<a class="changedPadd" href=" <?php echo $urltw; ?> ">
													<i class="fa fa-twitter iconPosTw"></i>	
												</a>
											</li>
										</ul>
									</menu>	
								</div>
							</div>	
							<div class="row marg">
								<div class="col-md-12">
									<!-- ZERO.kz -->
								<span id="_zero_68878">
								<noscript>
									<a href="http://zero.kz/?s=68878" target="_blank">
									<img src="http://c.zero.kz/z.png?u=68878" width="88" height="31" alt="ZERO.kz" />
									</a>
								</noscript>
									</span>

								<script type="text/javascript"><!--
									var _zero_kz_ = _zero_kz_ || [];
									_zero_kz_.push(["id", 68878]);
									_zero_kz_.push(["type", 1]);

									(function () {
									    var a = document.getElementsByTagName("script")[0],
									    s = document.createElement("script");
									    s.type = "text/javascript";
									    s.async = true;
									    s.src = (document.location.protocol == "https:" ? "https:" : "http:")
									    + "//c.zero.kz/z.js";
									    a.parentNode.insertBefore(s, a);
									})(); //-->
								</script>
								<!-- End ZERO.kz -->



								<!-- Yandex.Metrika informer -->
									<a href="https://metrika.yandex.kz/stat/?id=44211814&amp;from=informer"
									target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/44211814/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
									style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" class="ym-advanced-informer" data-cid="44211814" data-lang="ru" /></a>
									<!-- /Yandex.Metrika informer -->

									<!-- Yandex.Metrika counter -->
									<script src="https://mc.yandex.ru/metrika/watch.js" type="text/javascript"></script>
									<script type="text/javascript">
									try {
									    var yaCounter44211814 = new Ya.Metrika({
									        id:44211814,
									        clickmap:true,
									        trackLinks:true,
									        accurateTrackBounce:true,
									        webvisor:true
									    });
									} catch(e) { }
									</script>
									<noscript><div><img src="https://mc.yandex.ru/watch/44211814" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
									<!-- /Yandex.Metrika counter -->


									<!--LiveInternet counter--><script type="text/javascript">
										document.write("<a href='//www.liveinternet.ru/click' "+
										"target=_blank><img src='//counter.yadro.ru/hit?t38.6;r"+
										escape(document.referrer)+((typeof(screen)=="undefined")?"":
										";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
										screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
										";"+Math.random()+
										"' alt='' title='LiveInternet' "+
										"border='0' width='31' height='31'><\/a>")
										</script><!--/LiveInternet-->

									<!-- Rating@Mail.ru logo -->
									<a href="http://top.mail.ru/jump?from=2895840">
									<img src="//top-fwz1.mail.ru/counter?id=2895840;t=479;l=1" 
									style="border:0;" height="31" width="88" alt="Рейтинг@Mail.ru" /></a>
									<!-- //Rating@Mail.ru logo -->

								</div>
								
							</div>	

							<div class="row copyright">
								<div class="col-md-7"></div>
								<div class="col-md-5">
									<p> Copyright ©, Serik-s </p>
								</div>
							</div>			
																						
						</div>
					</div>

					
				</div>
			</div>
		</footer>
		<!-- END OF FOOTER PART -->
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

		<script type="text/javascript" src=" <?php echo base_url("assets/bootstrap/dist/js/bootstrap.js"); ?> "></script>

		<script type="text/javascript" src=" <?php echo base_url("assets/js/index.js"); ?> "></script>
 		<script type="text/javascript" src=" <?php echo base_url("assets/js/custom.js"); ?>"></script>
			

		<script type="text/javascript" src=" <?php echo base_url("/assets/js/index.js"); ?> "></script>	
		<script type="text/javascript" src=" <?php echo base_url("/assets/bootstrap/dist/js/bootstrap.js"); ?> "></script>
			

	</body>
</html>