<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="shortcut icon" href=" <?php echo base_url("assets/images/icon.png"); ?> ">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> Tennis Bro </title>

    <!-- Bootstrap Core CSS -->
    <link href=" <?php echo base_url("assets/landing-assets/lib/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet">

    <!-- Additional fonts for this theme -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href=" <?php echo base_url("assets/font-awesome/css/font-awesome.css"); ?>">


    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="<?php echo base_url("assets/landing-assets/lib/magnific-popup/magnific-popup.css"); ?>" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="<?php echo base_url("assets/landing-assets/css/creative.css"); ?>" rel="stylesheet">

    <!-- The main CSS -->
    <link rel="stylesheet" href="<?php echo base_url("assets/landing-assets/css/landing.css"); ?>">

    <!-- Temporary navbar container fix until Bootstrap 4 is patched -->
    <style>
    .navbar-toggler {
        z-index: 1;
    }
    
    @media (max-width: 576px) {
        nav > .container {
            width: 100%;
        }
    }
    </style>

</head>

<body id="page-top">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar fixed-top navbar-toggleable-md navbar-light">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarExample" aria-controls="navbarExample" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="container">
            <a class="navbar-brand page-scroll custom-brand" href="#page-top">Tennis Bro</a>
            <div class="collapse navbar-collapse" id="navbarExample">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link page-scroll custom-nav" href="#about">О нас</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll custom-nav" href="#services">Наша услуга</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll custom-nav" href="#portfolio">Галерея</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link page-scroll custom-nav" href="#contact">Контакты</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <header>
        <div class="header-content">
            <div class="header-content-inner">
                <h1 id="homeHeading">Говорят что ты интересуешься теннисом?</h1>
                <hr>
                <p>Это правда?</p>
                <a href="#about" class="btn btn-primary btn-xl page-scroll">Да!</a>
            </div>
        </div>
    </header>

    <section class="bg-primary" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 text-center">
                    <h2 class="section-heading text-white">
                        Мы знаем как тебе помочь! 
                        <img src=" <?php echo base_url("assets/landing-assets/img/smile1.png"); ?>" class="inline-smile" alt="">
                        <img src=" <?php echo base_url("assets/landing-assets/img/smile1.png"); ?>" class="inline-smile" alt="">
                        <img src=" <?php echo base_url("assets/landing-assets/img/smile1.png"); ?>" class="inline-smile" alt=""> 
                    </h2> 
                    <hr>
                    <p class="text-faded"> Твоя проблема выглядит довольно простой и мы определенно поможем тебе ее решить. Так что не парься и продолжай листать, и ты точно найдешь то что ты ищешь! </p>
                    <a href="#services" class="page-scroll btn btn-default btn-xl sr-button">Начать!</a>
                </div>
            </div>
        </div>
    </section>

    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="section-heading">Итак...</h2>
                    <p class="service-text">получается тебе нужен теннисный блог который удовлетворяет следующим условиям:</p>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-newspaper-o text-primary sr-icons"></i>
                        <h3>Содержательность</h3>
                        <p class="text-muted"> Интересные и оригинальные статьи про теннис! </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-picture-o text-primary sr-icons"></i>
                        <h3>Дизайн</h3>
                        <p class="text-muted"> Красивый и удобный дизайн на котором все понятно! </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-users text-primary sr-icons"></i>
                        <h3>Сообщество</h3>
                        <p class="text-muted">Пользователи которые могут делиться своими идеями и информацией о теннисе!</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-file-video-o text-primary sr-icons"></i>
                        <h3>Видео</h3>
                        <p class="text-muted">Ролики про смешные, улетные и захватывающие моменты в теннисе! </p>
                    </div>
                </div>
            </div>
            <hr class="primary">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="section-heading">
                        Хммм.... какой блог может вам подойти?  
                        <img src=" <?php echo base_url("assets/landing-assets/img/think-smile.png"); ?> " class="inline-smile" alt=""> 
                    </h2>
                </div>
            </div>
            <hr class="primary">    
            <div class="row">
                <div class="col-lg-3 col-md-6 text-center">
                    <h2 class="section-heading">Знаю!</h2>
                    <p class="service-text">Тебе нужен...</p>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <h2 class="section-heading">
                        <i class="fa fa-4x fa-hand-o-right text-primary sr-icons"></i>
                    </h2>
                    
                </div>
                <div class="col-lg-6 col-md-6 text-center">
                    <a href="http://tennis-bro.kz" class="important-text">
                        <h2 class="important-heading">
                            TENNIS-BRO
                            <img src=" <?php echo base_url("assets/landing-assets/img/cool.gif"); ?> " class="inline-important-smile" alt="">
                        </h2>
                    </a>
                    
                </div>
            </div>
        </div>
    </section>

    <section class="no-padding" id="portfolio">
        <div class="container-fluid">
            <div class="row no-gutter popup-gallery">
                <div class="col-lg-4 col-sm-6">
                    <a href="<?php echo base_url("assets/landing-assets/img/tennis-info.jpg"); ?> " class="portfolio-box">
                        <img src=" <?php echo base_url("assets/landing-assets/img/tennis-info.jpg"); ?> " class="img-fluid" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Контакты
                                </div>
                                <div class="project-name">
                                    Служба поддержки 24 часа в день
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="<?php echo base_url("assets/landing-assets/img/tennis-design.jpg"); ?> " class="portfolio-box">
                        <img src=" <?php echo base_url("assets/landing-assets/img/tennis-design.jpg"); ?> " class="img-fluid" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Дизайн
                                </div>
                                <div class="project-name">
                                    Удобный дизайн
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="<?php echo base_url("assets/landing-assets/img/tennis-content.jpg"); ?> " class="portfolio-box">
                        <img src=" <?php echo base_url("assets/landing-assets/img/tennis-content.jpg"); ?> " class="img-fluid" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Содержательность
                                </div>
                                <div class="project-name">
                                    Оригинальные тексты 
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="<?php echo base_url("assets/landing-assets/img/tennis-article.jpg"); ?> " class="portfolio-box">
                        <img src=" <?php echo base_url("assets/landing-assets/img/tennis-article.jpg"); ?> " class="img-fluid" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Статьи
                                </div>
                                <div class="project-name">
                                    Полезные статьи 
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="<?php echo base_url("assets/landing-assets/img/tennis-middle.jpg"); ?> " class="portfolio-box">
                        <img src=" <?php echo base_url("assets/landing-assets/img/tennis-middle.jpg"); ?> " class="img-fluid" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Турниры
                                </div>
                                <div class="project-name">
                                    Информация обо всех турнирах
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href=" <?php echo base_url("assets/landing-assets/img/tennis-video.jpg"); ?> " class="portfolio-box">
                        <img src=" <?php echo base_url("assets/landing-assets/img/tennis-video.jpg"); ?> " class="img-fluid" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Видео
                                </div>
                                <div class="project-name">
                                    Улетные моменты тенниса
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <aside class="bg-dark">
        <div class="container text-center">
            <div class="call-to-action">
                <h2>Удовлетворены?</h2>
                <a href="http://tennis-bro.kz" class="btn btn-default btn-xl sr-button">Присоединяйтесь!</a>
            </div>
        </div>
    </aside>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 text-center">
                    <h2 class="section-heading">Оставайтесь с нами!</h2>
                    <hr class="primary">
                    <p>По поводу сотрудничества онли дайрект</p>
                </div>
                <div class="col-lg-4 offset-lg-2 text-center">
                    <i class="fa fa-phone fa-3x sr-contact"></i>
                    <p>123-456-6789</p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                    <p><a href="mailto:web.application.creater@gmail.com">web.application.creater@gmail.com</a></p>
                </div>
            </div>
        </div>
    </section>

    <!-- jQuery Version 3.1.1 -->
    <script src=" <?php echo base_url("assets/landing-assets/lib/jquery/jquery.js"); ?> "></script>

    <!-- Tether -->
    <script src=" <?php echo base_url("assets/landing-assets/lib/tether/tether.min.js"); ?> "></script>

    <!-- Bootstrap Core JavaScript -->
    <script src=" <?php echo base_url("assets/landing-assets/lib/bootstrap/js/bootstrap.min.js"); ?> "></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src=" <?php echo base_url("assets/landing-assets/lib/scrollreveal/scrollreveal.min.js"); ?> "></script>
    <script src=" <?php echo base_url("assets/landing-assets/lib/magnific-popup/jquery.magnific-popup.min.js"); ?> "></script>

    <!-- Theme JavaScript -->
    <script src=" <?php echo base_url("assets/landing-assets/js/creative.min.js"); ?> "></script>

</body>

</html>