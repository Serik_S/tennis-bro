<html>
	<head>
		<link rel="stylesheet" href=" <?php echo base_url("assets/css/profile.css"); ?>">
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-5">
					<h4>Информация о пользователе</h4>
					<hr/>
					<div class="row">
						<div class="col-md-3">					
							<img src=" <?php echo base_url("assets/images/smile1.png"); ?> " class="profile-image" alt="">
						</div>
						<div class="col-md-9">
							<p class="fullname-field">Имя: <?php echo $uname; ?></p>
							<p>Электронная почта: <?php echo $uemail; ?></p>
						</div>
					</div>
				</div>
				<div class="col-md-1"></div>
				</div>
			</div>
	</body>
</html>
	