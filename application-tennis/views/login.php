<html>
  <head>
          <link rel="stylesheet" href=" <?php echo base_url("assets/css/login.css"); ?>">  
          <title> <?php echo $title; ?> </title>
  </head>
  <body>
    <div class="container">
            <div class="row marged">
              <div class="col-md-4 col-md-offset-4 well">
              <?php $attributes = array("name" => "loginform");
                echo form_open("login/index", $attributes);?>
                <legend>Вход</legend>
                <div class="form-group">
                  <label for="name">Email</label>
                  <input class="form-control" name="email" placeholder="Введите вашу электронную почту" type="text" value="<?php echo set_value('email'); ?>" />
                  <span class="text-danger"><?php echo form_error('email'); ?></span>
                </div>
                <div class="form-group">
                  <label for="name">Пароль</label>
                  <input class="form-control" name="password" placeholder="Введите пароль" type="password" value="<?php echo set_value('password'); ?>" />
                  <span class="text-danger"><?php echo form_error('password'); ?></span>
                </div>
                <div class="form-group">
                  <button name="submit" type="submit" class="btn btn-info tennis-color">Войти</button>
                  <button name="cancel" type="reset" class="btn btn-default">Отмена</button>
                </div>
              <?php echo form_close(); ?>
              <?php echo $this->session->flashdata('msg'); ?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-md-offset-4 text-center">  
              Новенький? Зарегистрируйтесь<a href="<?php echo base_url(); ?>registration"> здесь</a> 
              </div>
            </div>    
      </div>
  </body>
</html>