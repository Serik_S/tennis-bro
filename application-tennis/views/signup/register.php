<html>
	<head>
		<link rel="stylesheet" href=" <?php echo base_url("assets/css/registration.css"); ?> ">
	</head>
	<body>
		<div class="register-container container">
            <div class="row">
            	<div class="col-md-6">
	            	<div class="register span6">
	                    <form action="" method="post">
	                        <h2><span class="red"><strong>Присоединяйтесь :-) </strong></span></h2>                  
	                        <label for="fname">Имя</label>
	                        <input type="text" id="fname" name="fname" placeholder="Как вас зовут?">
	                        <label for="lname">Фамилия</label>
	                        <input type="text" id="lname" name="lname" placeholder="Ваша фамилия?">
	                        <label for="email">Email</label>
	                        <input type="text" id="email" name="email" placeholder="Введите вашу электронную почту...">
	                        <label for="password">Пароль</label>
	                        <input type="password" id="password" name="password" placeholder="введите ваш пароль">
	                        <label for="password">Подтверждение</label>
	                        <input type="password" id="password" name="password" placeholder="подтвердите!">
	                        <button type="submit">Зарегистрироваться!</button>
	                    </form>
	                </div>	
            	</div>                
                <div class="col-md-6">	
                		<img class="image-registration" src="images/keep-calm.png" alt="Не найдено :-(">
                </div>
            </div>
        </div>
	</body>
</html>
