<head>
	<link rel="stylesheet" href=" <?php echo base_url('assets/css/article-view.css') ?> ">
</head>
<div class="container">
	<div class="row">
		<div class="col-md-8">
			<h2 class="article-title"> <?php echo $articles_item['name']; ?> </h2>

		</div>
		<div class="col-md-4">
			
		</div>
	</div>
	<div class="row marged">
		<div class="col-md-8">
			<p class="article-text"> <?php echo $articles_item['information']; ?> </p>
		</div>
		<div class="col-md-4">
			<?php 
				$name_of_image = strval($articles_item['image']);
				$url = "assets/images/";
				$uri = $url . '' . $name_of_image;

			?>
			<?php if(strlen($name_of_image) > 1) : ?>
				<img class="img-responsive news-image" src=" <?php echo base_url("assets/images/$name_of_image"); ?>" alt="image not found">
			<?php endif; ?>
		</div>
	</div>
</div>


