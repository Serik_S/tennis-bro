<html>
	<head>
		<link rel="stylesheet" href=" <?php echo base_url("assets/css/articles.css"); ?> ">
		<meta name="description" content="Страница на которой содержаться все статьи, которые были написаны обычными пользователями или даже администраторами сайта">
		<meta property="og:title" content="Статьи про теннис" />
		<meta property="og:type" content="Теннисный блог" />
		<meta property="og:url" content="http://tennis-bro.kz" />
		<meta property="og:image" content="http://tennis-bro.kz/assets/images/winner.jpg" />

	</head>
	<body>
		<div class="container main">
				<div class="row">
		            <div class="col-md-12">
		                <h1 class="page-header">Мои Статьи
		                    <small>Здесь все самое интересное :-)</small>
		                </h1>
		            </div>
	        	</div>

				<div class="row padding">
					<div class="col-md-6 portfolio-item">

						<a href="#">
		                    <img class="img-responsive" src="<?php echo base_url("assets/images/winner.jpg"); ?>" alt="">
		                </a>
		                <h3>
		                    <a href="<?php echo site_url("articles/tennis-is-best"); ?> ">Теннис - лучшая игра в мире</a>
		                </h3>
		                <p>Статья о том почему теннис является одним из самых зрелищных видов спорта</p>
		                <a href=" <?php echo site_url("articles/tennis-is-best"); ?> " class="btn btn-default pulse">Читать</a>
		          
		            </div>
		            <div class="col-md-6 portfolio-item">
		                <a href="#">
		                    <img class="img-responsive" src="<?php echo base_url("assets/images/novak.jpg"); ?>" alt="">
		                </a>
		                <h3>
		                    <a href=" <?php echo site_url("articles/my-favourite-tennis-player"); ?> ">Мой любимый теннисист</a>
		                </h3>
		                <p> Новак Джокович является особенным игроком чей стиль игры совсем не похож на других, что делает его особенным игроком и моим кумиром </p>
		                <a href=" <?php echo site_url("articles/my-favourite-tennis-player"); ?> " class="btn btn-default">Читать</a>
		            </div>
				</div>

				<div class="row padding">
					<div class="col-md-6 portfolio-item">
		                <a href="#">
		                    <img class="img-responsive" src="<?php echo base_url("assets/images/melbourne.jpg"); ?>" alt="">
		                </a>
		                <h3>
		                    <a href="<?php echo site_url("articles/australian-open"); ?>">Australian Open</a>
		                </h3>
		                <p>Один из самых зрелищных турниров большого шлема, который проходит в Австралии</p>
		                <a href="<?php echo site_url("articles/australian-open"); ?>" class="btn btn-default">Читать</a>
		            </div>
		            <div class="col-md-6 portfolio-item">
		                <a href="#">
		                    <img class="img-responsive" src="<?php echo base_url("assets/images/roland.jpg"); ?>" alt="">
		                </a>
		                <h3>
		                    <a href="<?php echo site_url("articles/rolan-garros"); ?>">French Open - Roland Garros</a>
		                </h3>
		                <p>Грунтовый сезон, май, жаркое время. А турнир Ролан Гаррос проходящий в это время намного повышает температуру в воздухе. Также является одним из знаменитых турниров большого шлема, который проходит во Франции</p>
		                <a href="<?php echo site_url("articles/rolan-garros"); ?>" class="btn btn-default">Читать</a>
		            </div>

				</div>

				<div class="row padding">
					<div class="col-md-6 portfolio-item">
		                <a href="#">
		                    <img class="img-responsive" src="<?php echo base_url("assets/images/first.jpg"); ?>" alt="">
		                </a>
		                <h3>
		                    <a href="<?php echo site_url("articles/first-out-of-first"); ?>">Первые среди первых!</a>
		                </h3>
		                <p>В данной статье есть рассказывается о самых лучших теннисистах начиная с того момента как появился и сам теннис как вид спорта</p>
		                <a href="<?php echo site_url("articles/first-out-of-first"); ?>" class="btn btn-default">Читать</a>
		            </div>
		            <div class="col-md-6 portfolio-item">
		                <a href="#">
		                    <img class="img-responsive" src="<?php echo base_url("assets/images/federer.jpg"); ?> " alt="">
		                </a>
		                <h3>
		                    <a href="<?php echo site_url("articles/rodjer-federer"); ?>">Достижения Легендарного Роджера Федерера</a>
		                </h3>
		                <p>Роджер Федерер по настоящему легендарный. А вот почему он такой легендарный вы можете узнать только прочитав эту статью.</p>
		                <a href="<?php echo site_url("articles/rodjer-federer"); ?>" class="btn btn-default">Читать</a>
		            </div>
				</div>
			</div>
	</body>
</html>