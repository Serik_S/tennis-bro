<html>
    <head>
        <link rel="stylesheet" href=" <?php echo base_url("assets/bootstrap/dist/css/bootstrap.css"); ?>">    
        <link rel="stylesheet" href=" <?php echo base_url("assets/css/article-create.css"); ?>">
        <title> <?php echo $title; ?> </title>
    </head>
    <body>
        <div class="container">
            <div class="row">

                
                <div class="col-md-3"></div>
                <div class="col-md-5 article">

                <h2><?php echo $title; ?></h2>

                    <?php echo validation_errors(); ?>

                    <?php echo form_open('articles/create'); ?>
                    <form class="" action="" method="post">
            
                        <div class="form-group">
                            <label for="name">Название статьи</label>
                            <input class="form-control" type="input" name="name" /><br />
                        </div>
                        
                        <div class="form-group">
                            <label for="description">Краткое описание</label>
                            <textarea class="form-control" name="description" id="" ></textarea>
                        </div>
                        
                        <div class="form-group">
                            <label for="Information">Текст</label>
                            <textarea class="form-control" name="information" cols="30" rows="10"></textarea><br />
                        </div>

                        <div class="form-group">
                            <label for="Post-Author">Автор</label>
                            <input class="form-control" type="input" name="author">
                        </div>
                        
                        <div class="form-group">
                            <label for="slug">Английское название для ссылки</label>
                            <input class="form-control" type="input" name="slug">
                        </div>

        
                        

                        <input class="btn btn-success" type="submit" name="submit" value="Опубликовать!" />

                    </form>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </body>
</html>