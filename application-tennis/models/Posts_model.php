<?php
class Posts_Model extends CI_Model {

	public function __construct()
    {
            $this->load->database();
    }

    public function get_posts($slug = FALSE)
	{
        if ($slug === FALSE)
        {
                $query = $this->db->get('posts');
                return $query->result_array();
        }

        $query = $this->db->get_where('posts', array('Slug' => $slug));
	    return $query->row_array();
	}
	public function set_posts()
	{
	    $this->load->helper('url');

	    $slug = url_title($this->input->post('Slug'), 'dash', TRUE);

	    $data = array(
	        'Post-title' => $this->input->post('Post-title'),
	        'Slug' => $slug,
	        'Post-text' => $this->input->post('Post-text'),	
	        'Post-Author' => $this->input->post('Post-Author')	        
	    );

	    return $this->db->insert('posts', $data);
	}
}
?>