<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class articles_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
    }
	
	public function get_articles($slug = FALSE)
	{
        if ($slug === FALSE)
        {
                $query = $this->db->get('articles');
                return $query->result_array();
        }

        $query = $this->db->get_where('articles', array('slug' => $slug));
	    return $query->row_array();
	}
	
	public function set_articles()
	{
	    $this->load->helper('url');

	    $slug = url_title($this->input->post('slug'), 'dash', TRUE);	    

	    $data = array(
	        'name' => $this->input->post('name'),
	        'slug' => $slug,
	        'information' => $this->input->post('information'),	
	        'author' => $this->input->post('author')	        
	    );

	    return $this->db->insert('articles', $data);
	}
}?>