<?php
class Articles extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('articles_model');
                $this->load->helper('url_helper');
                $this->load->helper('url');
                $this->load->helper('html');
        }

        public function index()
        {
                $data['articles'] = $this->articles_model->get_articles();
                $data['title'] = 'Интересные статьи';

                $data['urlvk'] = 'http://vk.com/share.php?url=http://tennis-bro.kz/articles';
                $data['urlfa'] = 'http://www.facebook.com/sharer.php?u=http://tennis-bro.kz/articles';
                $data['urlli'] = 'https://www.linkedin.com/cws/share?url=http://tennis-bro.kz/articles';
                $data['urltw'] = 'http://twitter.com/share?url=http://tennis-bro.kz/articles';

                $this->load->view('templates/header', $data);
                $this->load->view('articles/index', $data);
                $this->load->view('templates/footer', $data);
        }
    
    
        public function view($slug = NULL)
        {
                $data['articles_item'] = $this->articles_model->get_articles($slug);

                $data['urlvk'] = 'http://vk.com/share.php?url=http://tennis-bro.kz/articles';
                $data['urlfa'] = 'http://www.facebook.com/sharer.php?u=http://tennis-bro.kz/articles';
                $data['urlli'] = 'https://www.linkedin.com/cws/share?url=http://tennis-bro.kz/articles';
                $data['urltw'] = 'http://twitter.com/share?url=http://tennis-bro.kz/articles';

                if (empty($data['articles_item']))
                {
                        show_404();
                }

                $data['title'] = $data['articles_item']['name'];

                $this->load->view('templates/header', $data);
                $this->load->view('articles/view', $data);
                $this->load->view('templates/footer', $data);
        }

        public function create()
        {
            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['title'] = 'Новая статья';

            $data['urlvk'] = 'http://vk.com/share.php?url=http://tennis-bro.kz/articles';
            $data['urlfa'] = 'http://www.facebook.com/sharer.php?u=http://tennis-bro.kz/articles';
            $data['urlli'] = 'https://www.linkedin.com/cws/share?url=http://tennis-bro.kz/articles';
            $data['urltw'] = 'http://twitter.com/share?url=http://tennis-bro.kz/articles';

            $this->form_validation->set_rules('name', 'name', 'required');
            $this->form_validation->set_rules('information', 'information', 'required');

            if ($this->form_validation->run() === FALSE)
            {
                $this->load->view('articles/create', $data);

            }
            else
            {
                $this->articles_model->set_articles();
                $this->load->view('articles/success');
            }
        }


}
?>