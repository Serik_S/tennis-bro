<?php
class Login extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url','html'));
		$this->load->library(array('session', 'form_validation'));
		$this->load->database();
		$this->load->model('user_model');
	}
    public function index()
    {
		// get form input
		$email = $this->input->post("email");
        $password = $this->input->post("password");

		// form validation
		$this->form_validation->set_rules("email", "Email-ID", "required");
		$this->form_validation->set_rules("password", "Password", "required");
		
		if ($this->form_validation->run() == FALSE)
        {
			// validation fail
			$data['title'] = 'Вход';

			$data['urlvk'] = 'http://vk.com/share.php?url=http://tennis-bro.kz';
            $data['urlfa'] = 'http://www.facebook.com/sharer.php?u=http://tennis-bro.kz';
            $data['urlli'] = 'https://www.linkedin.com/cws/share?url=http://tennis-bro.kz';
            $data['urltw'] = 'http://twitter.com/share?url=http://tennis-bro.kz';
			// fails
			$this->load->view('templates/header.php');
			$this->load->view('login', $data);
		}
		else
		{
			// check for user credentials
			$uresult = $this->user_model->get_user($email, $password);
			if (count($uresult) > 0)
			{
				// set session
				$sess_data = array('login' => TRUE, 'uname' => $uresult[0]->fname, 'uid' => $uresult[0]->id);
				$this->session->set_userdata($sess_data);
				redirect("profile");
			}
			else
			{
				$this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Wrong Email-ID or Password!</div>');
				redirect('login/index');
			}
		}
    }
}
?>