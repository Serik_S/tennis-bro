<?php
	
require('Login.php');

class profile extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','html'));
		$this->load->library(array('session', 'form_validation'));
		$this->load->database();
		$this->load->model('user_model');
	}

	private function isLoggedIn(){
		$user = wp_get_current_user();

		return $user->exists();
	}
	
	function index()
	{
		$details = $this->user_model->get_user_by_id($this->session->userdata('uid'));
		$data['uname'] = $details[0]->fname . " " . $details[0]->lname;
		$data['uemail'] = $details[0]->email;
		$data['title'] = $data['uname'];
		

		$data['urlvk'] = 'http://vk.com/share.php?url=http://tennis-bro.kz';
	    $data['urlfa'] = 'http://www.facebook.com/sharer.php?u=http://tennis-bro.kz';
	    $data['urlli'] = 'https://www.linkedin.com/cws/share?url=http://tennis-bro.kz';
	    $data['urltw'] = 'http://twitter.com/share?url=http://tennis-bro.kz';

		
		$this->load->view('templates/header', $data);
		$this->load->view('profile', $data);
		
	}
}

