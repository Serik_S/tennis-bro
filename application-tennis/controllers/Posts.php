<?php
class Posts extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('posts_model');
                $this->load->helper('url_helper');
                $this->load->helper('url');
                $this->load->helper('html');
        }

        public function index()
        {
                $data['posts'] = $this->posts_model->get_posts();
                $data['title'] = 'Главная страница';
                $data['urlvk'] = 'http://vk.com/share.php?url=http://tennis-bro.kz';
                $data['urlfa'] = 'http://www.facebook.com/sharer.php?u=http://tennis-bro.kz';
                $data['urlli'] = 'https://www.linkedin.com/cws/share?url=http://tennis-bro.kz';
                $data['urltw'] = 'http://twitter.com/share?url=http://tennis-bro.kz';


                $this->load->view('posts/index', $data);
                $this->load->view('templates/footer', $data);
        }

        public function about()
        {            
            $data['title'] = 'О нас';
            $data['urlvk'] = 'http://vk.com/share.php?url=http://tennis-bro.kz/about';
            $data['urlfa'] = 'http://www.facebook.com/sharer.php?u=http://tennis-bro.kz/about';
            $data['urlli'] = 'https://www.linkedin.com/cws/share?url=http://tennis-bro.kz/about';
            $data['urltw'] = 'http://twitter.com/share?url=http://tennis-bro.kz/about';


            $this->load->view('templates/header', $data);
            $this->load->view('posts/about');
            $this->load->view('templates/footer', $data);
        }

        

        public function videos()
        {
            $data['title'] = 'Видео';
            $data['urlvk'] = 'http://vk.com/share.php?url=http://tennis-bro.kz/videos';
            $data['urlfa'] = 'http://www.facebook.com/sharer.php?u=http://tennis-bro.kz/videos';
            $data['urlli'] = 'https://www.linkedin.com/cws/share?url=http://tennis-bro.kz/videos';
            $data['urltw'] = 'http://twitter.com/share?url=http://tennis-bro.kz/videos';

            $this->load->view('templates/header', $data);
            $this->load->view('posts/videos');
            $this->load->view('templates/footer', $data);
        }

        public function register()
        {
            $data['title'] = 'Регистрация';

            $data['urlvk'] = 'http://vk.com/share.php?url=http://tennis-bro.kz';
            $data['urlfa'] = 'http://www.facebook.com/sharer.php?u=http://tennis-bro.kz';
            $data['urlli'] = 'https://www.linkedin.com/cws/share?url=http://tennis-bro.kz';
            $data['urltw'] = 'http://twitter.com/share?url=http://tennis-bro.kz';

           
            $this->load->view('posts/register', $data);

        }
    
        public function view($slug = NULL)
        {
                $data['posts_item'] = $this->posts_model->get_posts($slug);

                $data['urlvk'] = 'http://vk.com/share.php?url=http://tennis-bro.kz';
                $data['urlfa'] = 'http://www.facebook.com/sharer.php?u=http://tennis-bro.kz';
                $data['urlli'] = 'https://www.linkedin.com/cws/share?url=http://tennis-bro.kz';
                $data['urltw'] = 'http://twitter.com/share?url=http://tennis-bro.kz';

                if (empty($data['posts_item']))
                {
                        show_404();
                }

                $data['title'] = $data['posts_item']['Post-title'];

                $this->load->view('templates/header', $data);
                $this->load->view('posts/view', $data);
        }

        public function create()
        {
            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['title'] = 'Ваша новость';
            $data['urlvk'] = 'http://vk.com/share.php?url=http://tennis-bro.kz';
            $data['urlfa'] = 'http://www.facebook.com/sharer.php?u=http://tennis-bro.kz';
            $data['urlli'] = 'https://www.linkedin.com/cws/share?url=http://tennis-bro.kz';
            $data['urltw'] = 'http://twitter.com/share?url=http://tennis-bro.kz';


            

            $this->form_validation->set_rules('Post-title', 'Title', 'required');
            $this->form_validation->set_rules('Post-text', 'Text', 'required');

            if ($this->form_validation->run() === FALSE)
            {
                $this->load->view('posts/create', $data);

            }
            else
            {
                $this->posts_model->set_posts();
                $this->load->view('posts/success');
            }
        }


}
?>