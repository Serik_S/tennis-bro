-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Мар 23 2017 г., 15:08
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `tennisdb`
-- 

-- --------------------------------------------------------

--
-- Структура таблицы `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(128) COLLATE utf8_bin NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_bin NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('1av854r8au7to1pmn2d595258hae960v', '127.0.0.1', 1490281356, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439303238313334373b);

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Post-title` varchar(120) COLLATE utf8_bin NOT NULL,
  `Slug` varchar(128) COLLATE utf8_bin NOT NULL,
  `Post-text` text COLLATE utf8_bin NOT NULL,
  `Post-Author` varchar(15) COLLATE utf8_bin NOT NULL,
  `image` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`ID`, `Post-title`, `Slug`, `Post-text`, `Post-Author`, `image`) VALUES
(1, 'Выбери величайшего теннисного персонажа в истории', 'my-first-post', 'Пройдите по ссылке чтобы голосовать за вашего любимого теннисиста', 'Serik_s', 'new1.jpg'),
(2, 'Джокович подкинул Медведева на Кубок Дэвиса на личном самолете', 'my-second-post', 'Интересный случай с Дмитрием Медведевым и известным теннистом Новаком Джоковичем', 'Serik_s', 'new2.jpg'),
(3, 'Тестовый проект', '0', 'Тестим slug', 'Me', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `fname`, `lname`, `email`, `password`) VALUES
(1, 'Serik', 'Seidigalimov', '2909Serik@gmail.com', '3fc0a7acf087f549ac2b266baf94b8b1'),
(2, 'Person', 'Person', 'mail@mail.ru', '25d55ad283aa400af464c76d713c07ad'),
(3, 'Persor', 'Persor', '123456@mail.ru', 'd8578edf8458ce06fbc5bb76a58c5ca4'),
(4, 'Serik', 'Seidigalimov', 'mail23@mail.com', '57ba172a6be125cca2f449826f9980ca'),
(5, 'ppp', 'ppe', 'pp@mail.ru', '315eb115d98fcbad39ffc5edebd669c9'),
(6, 'person', 'person', '909Serik@gmail.com', 'd8578edf8458ce06fbc5bb76a58c5ca4'),
(7, 'Person', 'Person', '223456@mail.ru', '827ccb0eea8a706c4c34a16891f84e7b'),
(8, 'Serik', 'Seidigalimov', 'qwert@mail.ru', 'd8578edf8458ce06fbc5bb76a58c5ca4'),
(9, 'Serik', 'Seidigalimov', 'qwert1@mail.ru', 'd8578edf8458ce06fbc5bb76a58c5ca4');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;



-- Это строка предназначена для проверки git на 
